package com.ptit.apiquanlidiem.service.impl;

import com.ptit.apiquanlidiem.converter.BangDiemChiTietConverter;
import com.ptit.apiquanlidiem.dto.*;
import com.ptit.apiquanlidiem.entity.BangDiemChiTietEntity;
import com.ptit.apiquanlidiem.entity.BangDiemChiTietKey;
import com.ptit.apiquanlidiem.entity.LopTinChiEntity;
import com.ptit.apiquanlidiem.entity.SinhVienEntity;
import com.ptit.apiquanlidiem.exception.DuplicateResourceException;
import com.ptit.apiquanlidiem.exception.ResourceNotFoundException;
import com.ptit.apiquanlidiem.repository.BangDiemChiTietRepository;
import com.ptit.apiquanlidiem.repository.LopTinChiRepository;
import com.ptit.apiquanlidiem.repository.SinhVienRepository;
import com.ptit.apiquanlidiem.service.BangDiemChiTietService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class BangDiemChiTietServiceImpl implements BangDiemChiTietService {

    @Autowired
    private BangDiemChiTietRepository bangDiemChiTietRepository;

    @Autowired
    private SinhVienRepository svRepository;

    @Autowired
    private BangDiemChiTietConverter bangDiemChiTietConverter;

    @Autowired
    private LopTinChiRepository lopTinChiRepository;

    @Autowired
    private ModelMapper modelMapper;


    @Override
    public List<BangDiemTheoKiDto> findAllByMSSV(String mssv) {

        SinhVienEntity sinhVien = this.svRepository.findOneByMssv(mssv.toUpperCase())
                .orElseThrow(() -> new ResourceNotFoundException("Sinh viên không tồn tại"));

        List<BangDiemChiTietEntity> listEntity
                = this.bangDiemChiTietRepository.findBySinhVien(sinhVien);

        if(listEntity==null){
            return null;
        }

        List<BangDiemTheoKiDto> response = new ArrayList<BangDiemTheoKiDto>();

        Map<String, List<TTBangDiemDto>> map = groupByHocKiAndNam(listEntity);

        int tinChiTichLuy=0;
        float tong4TichLuy=0;
        float tong10TichLuy=0;
        int tongTinChiDaDK=0;

        for(Map.Entry<String, List<TTBangDiemDto>> entry : map.entrySet()) {
            String[] key = entry.getKey().split("-");
            List<TTBangDiemDto> list = entry.getValue();
            int tinChiDat=0;
            float tong4=0;
            float tong10=0;
            int tongTinChi=0;
            for(TTBangDiemDto item: list){
                int tinChi= item.getSoTinChi();
                if(item.getKq().equals("Đạt")){
                    tinChiDat+= tinChi;
                }
                tongTinChi+=tinChi;
                tong4+= item.getDiemTrungBinhHe4()*tinChi;
                tong10+= item.getDiemTrungBinhHe10()*tinChi;
            }

            tongTinChiDaDK+=tongTinChi;
            tinChiTichLuy+=tinChiDat;
            tong4TichLuy+=tong4;
            tong10TichLuy+=tong10;

            BangDiemTheoKiDto dto = new BangDiemTheoKiDto();
            dto.setNam(Integer.parseInt(key[0]));
            dto.setHocKi(Integer.parseInt(key[1]));

            // Tính tích lũy từng kì
            dto.setTinChiDatHk(tinChiDat);
            dto.setTbHocKi4(((float) Math.round((float)
                    (tong4/tongTinChi)* 10)) / 10);
            dto.setTbHocKi10(((float) Math.round((float)
                    (tong10/tongTinChi)* 10)) / 10);

            // Tính tích lũy tất cả các kì tới học kì hiện tại
            dto.setTinChiTichLuy(tinChiTichLuy);
            dto.setTbTichLuy4(((float) Math.round((float)
                    (tong4TichLuy/tongTinChiDaDK)* 10)) / 10);
            dto.setTbTichLuy10(((float) Math.round((float)
                    (tong10TichLuy/tongTinChiDaDK)* 10)) / 10);

            dto.setListDiem(list);

            response.add(dto);
        }

        return response;
    }

    @Override
    public List<TTBangDiemDto> findAllByMSSVAndHKAndNam(String mssv, int hocKi, int nam) {
        SinhVienEntity sinhVien=this.svRepository.findOneByMssv(mssv.toUpperCase())
                .orElseThrow(() -> new ResourceNotFoundException("Sinh viên không tồn tại"));

        List<BangDiemChiTietEntity> listEntity
                = this.bangDiemChiTietRepository.findByMssvAndHocKiAndNam(mssv.toUpperCase(), hocKi, nam);

        return listEntity.size()==0
                ?null
                : listEntity.stream().map(item -> this.bangDiemChiTietConverter.toDto(item))
                .collect(Collectors.toList());
    }

    @Override
    public PageBangDiemResDto findAllBangDiemCTByMaLTC(String maLTC, int currentPage, int perPage) {

        Pageable paging = PageRequest.of(currentPage, perPage);

        LopTinChiEntity lopTinChi=this.lopTinChiRepository.findOneByMaLopTinChi(maLTC.toUpperCase())
                .orElseThrow(() -> new ResourceNotFoundException("Lớp tín chỉ không tồn tại"));

        Page<BangDiemChiTietEntity> pageListEntity
                = this.bangDiemChiTietRepository.findByLopTinChi(lopTinChi, paging);

        return pageListEntity.getTotalElements() > 0
                ? new PageBangDiemResDto(pageListEntity.getTotalPages(),
                pageListEntity.getTotalElements(),
                pageListEntity.getSize(),
                pageListEntity.getNumberOfElements(),
                pageListEntity.getNumber() + 1,
                pageListEntity.isFirst(),
                pageListEntity.isLast(),
                pageListEntity.getContent().stream()
                        .map(item -> this.bangDiemChiTietConverter.toDto(item))
                        .collect(Collectors.toList()))

                : null;
    }

    @Override
    public TTBangDiemDto findOneByMSSVAndMaLTC(String mssv, String maLTC) {
        BangDiemChiTietEntity entity = this.bangDiemChiTietRepository.findOneById(new BangDiemChiTietKey(mssv.toUpperCase(), maLTC))
                .orElseThrow(()->new ResourceNotFoundException("Thông tin không hợp lệ"));
        return this.bangDiemChiTietConverter.toDto(entity);

    }

    //đăng kí môn học
    @Override
    @Transactional
    public int save(BangDiemChiTietDto bangDiemChiTiet) {

        LopTinChiEntity ltcEntity = this.lopTinChiRepository.findOneByMaLopTinChi(bangDiemChiTiet.getId().getMaLTC().toUpperCase())
                .orElseThrow(()-> new ResourceNotFoundException("Không tồn tại lớp tín chỉ này"));
        this.svRepository.findOneByMssv(bangDiemChiTiet.getId().getMssv().toUpperCase())
                .orElseThrow(()-> new ResourceNotFoundException("Không tồn tại sinh viên này"));

        if(ltcEntity.getTrangThai() && ltcEntity.getSlDangKi()<ltcEntity.getSoSVToiDa()) {

            BangDiemChiTietEntity diemEntity
                    = this.bangDiemChiTietRepository.findOneById(
                            new BangDiemChiTietKey(bangDiemChiTiet.getId().getMssv().toUpperCase()
                                    ,bangDiemChiTiet.getId().getMaLTC().toUpperCase())).orElse(null);
            if(diemEntity!=null){
                throw new DuplicateResourceException("Bạn đã đăng kí môn này");
            }
            this.bangDiemChiTietRepository.save(modelMapper.map(bangDiemChiTiet, BangDiemChiTietEntity.class));
            ltcEntity.setSlDangKi(ltcEntity.getSlDangKi()+1);
            this.lopTinChiRepository.save(ltcEntity);
            return 1;

        }else {
            return 0;
        }
    }

    @Override
    public void update(BangDiemChiTietDto bangDiemChiTiet) {
        bangDiemChiTiet.getId().setMssv(bangDiemChiTiet.getId().getMssv().toUpperCase());
        bangDiemChiTiet.getId().setMaLTC(bangDiemChiTiet.getId().getMaLTC().toUpperCase());
        BangDiemChiTietEntity entity
                = bangDiemChiTietRepository.findOneById(
                        new BangDiemChiTietKey(bangDiemChiTiet.getId().getMssv(),bangDiemChiTiet.getId().getMaLTC()))
                .orElseThrow(()-> new ResourceNotFoundException("Sinh viên không học môn này"));
        this.bangDiemChiTietRepository.save(modelMapper.map(bangDiemChiTiet, BangDiemChiTietEntity.class));
    }


    private Map<String, List<TTBangDiemDto>> groupByHocKiAndNam(List<BangDiemChiTietEntity> list) {
            Map<String, List<TTBangDiemDto>> grouped = new HashMap<>();

            for (BangDiemChiTietEntity item : list) {
                String key = item.getLopTinChi().getNam() + "-" + item.getLopTinChi().getHocKi();

                if (!grouped.containsKey(key)) {
                    grouped.put(key, new ArrayList<>());
                }

                grouped.get(key).add(this.bangDiemChiTietConverter.toDto(item));
            }

            return grouped;
        }

}
