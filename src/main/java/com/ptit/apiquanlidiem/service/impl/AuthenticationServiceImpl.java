package com.ptit.apiquanlidiem.service.impl;


import com.ptit.apiquanlidiem.dto.RequestLoginDto;
import com.ptit.apiquanlidiem.dto.ResponseLoginDto;
import com.ptit.apiquanlidiem.entity.AccountEntity;
import com.ptit.apiquanlidiem.exception.NotActivatedExceptionHandler;
import com.ptit.apiquanlidiem.exception.ResourceNotFoundException;
import com.ptit.apiquanlidiem.repository.AccountRepository;
import com.ptit.apiquanlidiem.util.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;


@Service
public class AuthenticationServiceImpl {

    @Autowired
    private AccountRepository accRepo;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwt;


    @Autowired
    private PasswordEncoder passwordEncoder;

    public ResponseLoginDto authenticate(RequestLoginDto request) throws UnsupportedEncodingException {
        request.setUsername(request.getUsername().toUpperCase());
        AccountEntity user = this.accRepo
                .findOneByUsername(request.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException("User with email " + request.getUsername() + " not found!"));
        if (!user.getStatus()) {
            throw new NotActivatedExceptionHandler("Your account is not activated!");
        }

        if(!passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            throw new IllegalArgumentException("Sai username hoặc mật khẩu!");
        }

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(request.getUsername() ,
                        request.getPassword()));

        String accessToken = jwt.generateAccessToken(user);

        String refreshToken =jwt.generateRefreshToken(user);

        return new ResponseLoginDto(accessToken,refreshToken, user.getRole().getId());

    }
}
